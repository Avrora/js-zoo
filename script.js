enclos1 = ["aligator: Bob", "aligator: Dilan"];
enclos2 = ["kangourou: Ali", "kangourou: Arto", "kangourou: Mato"];
enclos3 = ["singe: Kami", "singe: Maugli"];
enclos4 = ["lama: Otto"];
enclos5 = ["tigre: Ernesto", "tigre: Roque", "tigre: Riska"];
allEnclos = [enclos1, enclos2, enclos3, enclos4, enclos5];

//Pokazivaet zivotnih v odnom voliere
function listerAnimauxEnclo(unEnclo) {
    for (i=0; i<=unEnclo.length-1; i++) {
        let animal = unEnclo[i];
        console.log(animal);
    }
    
}

listerAnimauxEnclo(enclos1);
listerAnimauxEnclo(enclos2);
listerAnimauxEnclo(enclos3);
listerAnimauxEnclo(enclos4);
listerAnimauxEnclo(enclos5);


//Pokazivaet zivotnih vo vseh volierah
function listerAnimauxEnclos(tabEnclos) {
    for (i=0; i<=tabEnclos.length-1; i++) {
        listerAnimauxEnclo(allEnclos);
    }
}

listerAnimauxEnclos(allEnclos);